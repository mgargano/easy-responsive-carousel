<?php


/**
 * Class easy_carousel
 */
class Easy_carousel {

	/**
	 * @var string
	 */
	public static $post_type = 'easy_carousel';
	/**
	 * @var string
	 */
	public static $file_name = 'easy-carousel';
	/**
	 * @var string
	 */
	public static $shortcode = 'easy_carousel';
	/**
	 * @var int
	 */
	public static $incrementer = 0;
	/**
	 * @var string
	 */
	public static $ver = '0.4.1';


	/**
	 * initialize the plugin
	 *
	 * @return void
	 *
	 */
	public static function init() {
		add_action( 'init', array( __CLASS__, 'register_post_type' ) );

		add_action( 'wp_enqueue_scripts', array( __CLASS__, 'enqueue' ) );
		add_shortcode( self::$shortcode, array( __CLASS__, 'shortcode' ) );

	}

	/**
	 *
	 * Register the post type
	 *
	 * @return void
	 *
	 */
	public static function register_post_type() {
		$labels = array(
			'name'               => 'Easy Carousel',
			'singular_name'      => 'Easy Carousel',
			'add_new'            => 'Add New',
			'add_new_item'       => 'Add New Carousel',
			'edit_item'          => 'Edit Carousel',
			'new_item'           => 'New Carousel',
			'all_items'          => 'All Carousels',
			'view_item'          => 'View Carousel',
			'search_items'       => 'Search Carousels',
			'not_found'          => 'No Carousels found',
			'not_found_in_trash' => 'No Carousels found in Trash',
			'parent_item_colon'  => '',
			'menu_name'          => 'Easy Carousel'
		);

		$args = array(
			'labels'       => $labels,
			'public'       => false,
			'show_in_menu' => true,
			'show_ui'      => true,
			'hierarchical' => true,
			'has_archive'  => true,
			'rewrite'      => false,
			'supports'     => array( 'title', 'editor', 'thumbnail', 'page-attributes' ),
		);

		register_post_type( self::$post_type, $args );
	}

	/**
	 *
	 * Shortcode method
	 *
	 * @param $atts
	 *
	 * @return string
	 */
	public static function shortcode( $atts ) {

		/* initialize variables */
		$id = $timeout = $hover_pause = $clear_after = $effect = $image_size = $orderby = $order = $mobile = $caption = $caption_opacity = $indicators = $arrows = null;
		extract( shortcode_atts( array(
			'id'              => - 1,
			'timeout'         => 5000,
			'hover_pause'     => false,
			'effect'          => '',
			'orderby'         => 'menu_order',
			'order'           => 'asc',
			'mobile'          => true,
			'caption'         => true,
			'caption_opacity' => '0.8',
			'indicators'      => true,
			'arrows'          => false,
			'image_size'      => 'full',
			'clear_after'     => true
		), $atts ) );
		if ( ! $mobile && wp_is_mobile() ) {
			return false;
		}
		static::$incrementer ++;
		$inner_html = $pre_outer_html = $post_outer_html = $html = $pause_att = '';
		$counter    = 0;

		if ( $id == - 1 || ! get_post( $id ) ) {
			return false;
		}
		if ( ! self::$post_type == get_post_type( $id ) ) {
			return false;
		}
		$clear = 'cf';
		if ( ! $clear_after ) {
			$clear = '';
		}

		$pre_outer_html .= '<div class="flexslider ' . $clear . ' easy-responsive-carousel carousel" id="carousel-' . static::$incrementer . '"><ul class="slides">';
		$children = get_posts( array(
			'post_type'   => self::$post_type,
			'post_parent' => $id,
			'orderby'     => $orderby,
			'order'       => $order
		) );

		foreach ( $children as $post ) : setup_postdata( $post );
			$counter ++;
			$thumbnail_id = get_post_thumbnail_id( $post->ID, $image_size );
			$image_array  = wp_get_attachment_image_src( $thumbnail_id, $image_size );
			$image        = $image_array[0];
			$inner_html .= '<li>';
			$inner_html .= sprintf( '<img src="%s" alt="%s">', $image, get_the_title() );
			if ( $caption && get_the_content() ) :
				$text_style       = '';
				$background_color = get_post_meta( wp_get_post_parent_id( $post->ID ), 'content_color', true );
				$opacity          = get_post_meta( wp_get_post_parent_id( $post->ID ), 'opacity', true );
				$text_color       = get_post_meta( wp_get_post_parent_id( $post->ID ), 'text_color', true );
				if ( is_numeric( $opacity ) ) {
					$caption_opacity = $opacity;
				}
				$text_color = $text_color ? $text_color : '#FFF';

				$background_color = $background_color ? $background_color : '#000';

				$caption_opacity = $caption_opacity ? $caption_opacity : 0.5;

				if ( $text_color ) {
					$text_style = 'color:' . $text_color;
				}

				$rgb         = hex2rgb( $background_color );
				$color_style = 'rgba(' . $rgb[0] . ',' . $rgb[1] . ',' . $rgb[2] . ', ' . $caption_opacity . ')';

				$inner_html .= '<div class="content" style="background:' . $color_style . ';' . $text_style . '">';
				$inner_html .= get_the_content();
				$inner_html .= '</div></li>';

			endif;

			wp_reset_postdata();
		endforeach;
		$inner_html .= '</ul>';
		$inner_html .= '</div>';

		ob_start();
		?>
		<script>jQuery(".carousel#carousel-<?php echo static::$incrementer; ?>").flexslider({
				slideshowSpeed: <?php echo $timeout; ?><?php if ( $hover_pause ) : ?>,
				pauseOnHover  : true <?php endif; ?><?php if ( ! $arrows ) : ?>,
				directionNav  : false <?php endif; ?><?php if ( $effect ) : ?>,
				effect        : <?php echo $effect; ?> <?php endif; ?><?php if ( ! $indicators ) : ?>,
				controlNav    : false <?php endif; ?>
			});</script><?php
		$post_outer_html .= ob_get_clean();

		$html = $pre_outer_html . $inner_html . $post_outer_html;

		return $html;
	}


	/**
	 *
	 * Enqueue scripts
	 *
	 * @return void
	 *
	 */
	static function enqueue() {
		global $post;
		if ( ! empty( $post ) ) {
			wp_enqueue_style( self::$file_name, plugins_url( 'css/' . self::$file_name . '.css', dirname( __FILE__ ) ), false, self::$ver );
			wp_enqueue_style( 'flexslider', plugins_url( 'css/flexslider.css', dirname( __FILE__ ) ), false, self::$ver );
			wp_enqueue_script( 'flexslider', plugins_url( 'js/jquery.flexslider-min.js', dirname( __FILE__ ) ), array( 'jquery' ), self::$ver );


		}
	}


}

